import { Component, OnInit } from '@angular/core';

import { Car } from './car';
import { CarService } from './car.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/*
To be able to display the data to the client,
you need a method inside the component which subscribes
to the data that the service fetches from the serve
*/
export class AppComponent implements OnInit {
  title = 'My Wonderful App';
  cars: Car[]; // stores an array of cars retrieved from the server
  error = '';
  success = '';
  car = new Car('', 0); // first value for the model name, second for the price

  // carService is injected so that the service will be available to all the methods of the controller
  constructor(private carService: CarService) {
  }

  // special lifecycle hook that runs immediately after the constructor finishes injecting dependencies
  ngOnInit() {
    this.getCars();
  }

  /* The method gets the cars list from the server
  & subscribes them to the getAll() method in the service */
  getCars(): void {
    this.carService.getAll().subscribe(
      // this first callback handles the successful retrieval of the list
      (res: Car[]) => {
        this.cars = res;
      },
      // this second callback handles errors
      (err) => {
        this.error = err;
      }
    );
  }

  /* The method gets values from the form
  & subscribes them to the store() method in the service */
  addCar(f) {
    this.error = '';
    this.success = '';

    // thanks to store(),, the service gets entered data from the user towards the form
    this.carService.store(this.car)
      .subscribe(
        (res: Car[]) => {
          // update the list of cars
          this.cars = res;

          // inform the user
          this.success = 'Created successfully';

          // reset the form
          f.reset();
        },
        (err) => this.error = err
      );
  }

  /* The method gets the values from the form
  & subscribes them to the update method in the service */
  updateCar(name, price, id) {
    this.success = '';
    this.error = '';

    this.carService.update({ model: name.value, price: price.value, id: +id})
      .subscribe(
        (res) => {
          this.cars = res;
          this.success = 'Updated successfully';
        },
        (err) => this.error = err
      );
  }

  /* The method gets the id from the form
  & subscribes it to the delete() method in the service */
  deleteCar(id) {
    this.success = '';
    this.error = '';

    this.carService.delete(+id)
      .subscribe(
        (res: Car[]) => {
          this.cars = res;
          this.success = 'Deleted successfully';
        },
        (err) => this.error = err
      );
  }

}
