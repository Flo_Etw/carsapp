import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';

/*
Observables are used to treat asynchronous code.
Ex : listenning to events & navigating between the page of an app (routing)
There is an Observable & an Observer
Observable sends data (in response to an event) | Observer subscribes to it to receive the data.
Observer has three handles to use the data : onNext (handles requested data), onError & onComplete (used at the end of the process)
*/
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Car } from './car';

// allows the service to participate in the dependency injection system
@Injectable({
  providedIn: 'root'
})
export class CarService {
  baseUrl = 'http://localhost/api';
  cars: Car[];

  constructor(private http: HttpClient) { } // dependency injection in the constructor

  // return all the cars wrapped in an Observable
  getAll(): Observable<Car[]> {
    // get() method is used to fetch the data from the server side
    // pipe() method is used to extract the list of cars
    return this.http.get(`${this.baseUrl}/list`).pipe(
      map((res) => {
        const key = 'data';
        this.cars = res[key];
        return this.cars;
      }),
      catchError(this.handleError)
    );
  }

  store(car: Car): Observable<Car[]> {
    // post() used to send the data to the server side
    // the first param is the URL & the second is the car object
    return this.http.post(`${this.baseUrl}/store`, { data: car})
      .pipe(map((res) => {
        const key = 'data';
        this.cars.push(res[key]);
        return this.cars;
      }),
      catchError(this.handleError));
  }

  update(car: Car): Observable<Car[]> {
    // put() method accepts the URL as the first parameter and the data (car object) as the second.
    return this.http.put(`${this.baseUrl}/update`, { data: car})
      .pipe(map((res) => {
        /* find method on the list of cars to search for the car that needs to be updated,
        before updating it with the data entered by the user */
        const theCar = this.cars.find((item) => {
          const id = 'id';
          return +item[id] === +car[id];
        });
        if (theCar) {
          const price = 'price';
          const model = 'model';
          theCar[price] = +car[price];
          theCar[model] = car[model];
        }
        return this.cars;
      }),
      catchError(this.handleError));
  }

  delete(id: number): Observable<Car[]> {
    const params = new HttpParams()
      .set('id', id.toString());
      // delete() method accepts the URL as the first parameter and the data (id) as the second
    return this.http.delete(`${this.baseUrl}/delete`, { params: params})
        .pipe(map(res => {
          // the delete() method needs to returned the updated cars list
          // filter will return all the cars except the deleted one
          const filteredCars = this.cars.filter((car) => {
            const key = 'id'
            return +car[key] !== +id;
          });
          return this.cars = filteredCars;
        }),
        catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    console.log(error);
    // return an Observable with a user message
    return throwError('Error! Something went wrong.');
  }
}
