# Cars

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## REST server

This project runs with a PHP backend. You can find it at this link `https://gitlab.com/Flo_Etw/phprestserver`
